import * as React from 'react';
import Home from './home/home';
import Results from './results/results';
import LoginPage from './login/login';
import LogoutPage from './logout/logout';
import { CoinInfo } from './types/CoinInfo'
import axios from 'axios'
import { AppToaster } from './utils/Toaster'
import HistoryPage from './history/history';
import { Moment } from 'moment';
import { ClipLoader } from 'react-spinners';
import { SearchHistory } from './types/SearchHistory';
import BlogHome from './blog/blogHome';
import BlogPost from './blog/blogPost';
import FAQ from './faq/faq';

import { getCurrentPrice, getPreviousPrice } from './store/profitStore'
import { CoinHistory } from './types/CoinHistory';
import Header from './header/header';
import { Total } from './types/Total';
import { DatabaseReference, FirebaseDatabase } from 'firebase/database';
import { app, base } from './base';
import './App.css';

import {
  BrowserRouter as Router,
  Route,
  withRouter,
  Redirect,
  Switch,
} from 'react-router-dom';
import { Search } from 'history';

export type Props = {};

export type coinResult = {
  price: string;
  date: Moment;
  coinSym: string;
};

export type State = {
  data: Total | null;
  loading: boolean;
  coinTypes: CoinInfo[];
  coinNames: any[];
  canCalculate: boolean,
  currentUser: { uid: string; histories: {} };
  histories: Total[];
  coinSym: string;
  price: string;
  error: { errorHappened: boolean; errorMsg: string };
  currentPrice: any;
  coinHistory: CoinHistory[];
  user: any;
  total: {
    newCP?: number;
    CP?: number;
    newSP?: number;
    SP?: number;
    lostPercent?: number;
  };
  authenticated: boolean;
  redirect: boolean;
};

class App extends React.Component<Props, State> {
  private removeAuthListener;
  private actionHistories;

  constructor(props: Props) {
    super(props);
    this.state = {
      coinHistory: [],
      authenticated: false,
      coinNames: [],
      coinTypes: [],
      canCalculate: false,
      currentUser: { uid: '', histories: {} },
      error: { errorHappened: false, errorMsg: '' },
      price: '',
      histories: [],
      coinSym: '',
      data: null,
      user: [],
      loading: false,
      currentPrice: [],
      redirect: false,
      total: { newCP: 0, CP: 0, newSP: 0, SP: 0, lostPercent: 0 },
    };
  }

  getPriceandDate(
    price: string,
    image: string,
    date: Moment,
    coinSym: string,
    currency: string
  ) {
    this.setState({
      loading: true,
    });

    getCurrentPrice(coinSym, currency)
      .then(response => {
        this.setState({
          currentPrice: response.data[`${currency}`],
        }, () => {
          getPreviousPrice(coinSym, currency, date)
            .then(res => {
              let values = { 'EUR': res.data[`${coinSym}`].EUR, 'USD': res.data[`${coinSym}`].USD } as Total

              this.setState(
                {
                  error: { errorHappened: false, errorMsg: '' },
                  data: values,
                },
                () => {
                  if (response.data.Response !== 'Error') {
                    this.calculateProfit(currency, price, coinSym, image);
                  } else {
                    this.setState({
                      loading: false,
                      error: { errorHappened: true, errorMsg: res.data.Message },
                    });
                  }
                }
              );
            })
            .catch(error => {
              this.showToast('')
              this.setState({
                error: { errorHappened: true, errorMsg: error.message, loading: false },
              });
            }).then(() =>
              this.setState({ loading: false })
            );
        });
      })
      .catch(error => {
        this.setState({
          error: { errorHappened: true, errorMsg: error.message, loading: false },
        });
      });

  }

  calculateProfit(currency: string, price: string, coinSym: string, image: string) {
    let CP: number = 0;
    if (this.state.data) CP = this.state.data[`${currency}`];

    let newCP = parseInt(price, undefined) * 100;
    newCP = newCP * CP / 100;

    const SP: number = this.state.currentPrice;
    let newSP = parseInt(price, undefined) * 100;
    newSP = newSP * SP / 100;

    if (newCP < newSP) {
      var gain = newSP - newCP;
      var gainPercent = gain / CP * 100;

      const unableToCalculate = gainPercent === Number.POSITIVE_INFINITY;

      if (unableToCalculate) {
        this.showToast("Unable to calculate profit please try another coin.");
        this.setState({ canCalculate: false, loading: false })
        return
      }

      let total = {
        coinAmount: price,
        gainPercent: gainPercent,
        coinImage: image,
        currency: currency,
        symbol: coinSym,
        newCP: newCP,
        CP: CP,
        newSP: newSP,
        values: this.state.data,
        SP: SP,
      } as Total;

      this.setState({
        loading: false,
        canCalculate: true,
        total: total,
      });
    } else {
      let loss = CP - SP;
      var lossPercent = loss / CP * 100;
      let totalLoss = {
        coinAmount: price,
        CP: parseInt(price, undefined),
        currency: currency,
        symbol: coinSym,
        coinImage: image,
        newCP: newCP,
        values: this.state.data,
        SP: SP,
        lostPercent: lossPercent,
        newSP: newSP,
      } as Total;

      this.setState({
        loading: false,
        canCalculate: true,
        total: totalLoss,
      });
    }
  }

  showToast = (message: string) => {
    // create toasts in response to interactions.
    // in most cases, it's enough to simply create and forget (thanks to timeout).
    AppToaster.show({ message: "Unable to calculate profit please try another coin." });
  }

  componentWillUnMount() {
    this.removeAuthListener = app.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({
          authenticated: true,
          loading: false,
        });
      } else {
        this.setState({
          authenticated: false,
          loading: false
        });
      }
    });
    base.removeBinding(this.actionHistories);
  }

  componentWillMount() {
    this.removeAuthListener = app.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({
          authenticated: true,
          loading: false,
          currentUser: user,
          user: user,
        });

        this.actionHistories = base.syncState(`searchHistory/${user.uid}`, {
          context: this,
          state: 'histories',
        });

      } else {
        this.setState({
          currentUser: { uid: '', histories: {} },
          authenticated: false,
        });
        // base.removeBinding(this.actionHistories);
      }
    });
  }

  private setCurrentUser(user) {
    if (user) {
      this.setState({
        currentUser: user,
        authenticated: true,
      });
    } else {
      this.setState({
        currentUser: { uid: '', histories: {} },
        authenticated: false,
      });
    }
  }

  private addHistory(history: Total) {
    const histories = { ...this.state.histories };
    const id = Date.now();

    histories[id] = {
      ...history,
      owner: this.state.currentUser.uid,
    } as Partial<SearchHistory>;

    this.setState({ histories: histories });
  }

  getCoinTypes() {
    let coins: any[] = []
    let coinNames: string[] = [];

    this.setState({
      loading: true,
    })

    axios
      .get(
        'https://rocky-bayou-96357.herokuapp.com/https://www.cryptocompare.com/api/data/coinlist/'
      )
      .then(response => {
        Object.keys(response.data.Data).forEach(function (key) {
          coins.push(response.data.Data[key])
        })

        coins.forEach(element => {
          coinNames.push(element.CoinName)
        })

        coinNames.sort()
        coins.sort()

        this.setState({
          coinNames: coinNames,
          coinTypes: coins,
          loading: false,
        })
      })
      .catch(function (error) { })
  }

  setItem(item: Total) {
    let total = {};
    this.setState({ total: item, redirect: true })
  }

  onLocationChange() {
    this.setState({ redirect: false })
  }

  render() {

    const results = { data: this.state.data, onSaveHistory: histories => this.addHistory(histories), error: this.state.error, loading: this.state.loading, total: this.state.total }

    const HomePage = (props) => (
      <Home
        results={results}
        isLoading={this.state.loading}
        getCoinTypes={() => this.getCoinTypes()}
        coinTypes={this.state.coinTypes}
        coinNames={this.state.coinNames}
        canCalculate={this.state.canCalculate}
        fireBaseApp={app}
        history={props.history}
        onSearch={(date, image, price, coinSym, currency) => {
          this.getPriceandDate(date, image, price, coinSym, currency);
        }}
      >
        Dashboard
      </Home>
    );
    const Logout = () => <LogoutPage app={app} />;

    const faq = () => <FAQ />;

    const HistoryList = () => (
      <HistoryPage
        totalProfits={this.state.histories}
        onSelectItem={(item: Total) => this.setItem(item)}
      />
    );

    const Login = () => (
      <Route
        exact={true}
        path="/login"
        render={props => {
          return (
            <LoginPage
              setCurrentUser={user => this.setCurrentUser(user)}
              location={props.location}
              isAuthenticated={this.state.authenticated}
              app={app}
              {...props}
            />
          );
        }}
      />
    );

    const ResultsPage = () => (
      <Results
        onSaveHistory={histories => this.addHistory(histories)}
        data={this.state.data}
        error={this.state.error}
        loading={this.state.loading}
        total={this.state.total}
      >
        Dashboard
      </Results>
    );

    const AuthenticatedRoute = ({
      component: Component,
      authenticated,
      ...rest
    }) => {
      return (
        <Route
          path='/history'
          {...rest}
          render={props =>
            authenticated === true ? (
              <Component {...props} {...rest} />
            ) : (
                <Redirect

                  to={{ pathname: '/login', state: { from: props.location } }}
                />
              )
          }
        />
      );
    };

    return (
      <Router>
        <div>

          <div className="dashboard">
            <Header
              onLocationChange={() => this.onLocationChange()}
              isAuthenticated={this.state.authenticated}
              userEmail={this.state.user.email}
            />
            <div className="main">
              <Switch key={location.pathname} >
                <Route 
                  exact={true}
                  path="/" 
                  render={(props) =>
               <Home
                  results={results}
                  getCoinTypes={() => this.getCoinTypes()}
                  coinTypes={this.state.coinTypes}
                  coinNames={this.state.coinNames}
                  canCalculate={this.state.canCalculate}
                  isLoading={this.state.loading}
                  fireBaseApp={app}
                  history={props.history}
                  onSearch={(date, image, price, coinSym, currency) => {
                    this.getPriceandDate(date, image, price, coinSym, currency);
                  }}
               />
                } 
                />
                <Route 
                  path="/home" 
                  render={(props) => 
                <Home
                  results={results}
                  getCoinTypes={() => this.getCoinTypes()}
                  coinTypes={this.state.coinTypes}
                  coinNames={this.state.coinNames}
                  canCalculate={this.state.canCalculate}
                  isLoading={this.state.loading}
                  fireBaseApp={app}
                  history={props.history}
                  onSearch={(date, image, price, coinSym, currency) => {
                    this.getPriceandDate(date, image, price, coinSym, currency);
                  }}
                />
                }
                />
                <Route path="/logout" component={Logout} />
                <Route path="/p/:page" component={BlogHome} />
                <Route path="/post/:slug:id" component={BlogPost} />
                <Route path="/faq" component={faq} />
                <Route path="/login" component={Login} />
                <AuthenticatedRoute
                  path="/history"
                  authenticated={this.state.authenticated}
                  component={HistoryList}
                />
                <Route path="/results" component={ResultsPage} />

              </Switch>  
            </div>

          </div>        
          <footer className="">
              <ul>
                  <li>Data from <a href="www.cryptocompare.com"> CryptoCompare </a></li>
                  <li>
                  <a
                    className="submit"
                    href="mailto:dunnedev@jsdiaries.com?Subject=Advertising%query"
                  >
                    Contact
                  </a>
                  </li>
                  </ul>
                </footer>
        </div>

      </Router>
    );
  }
}

export default App;
