import * as React from 'react'

export type State = {}

export type Props = {
  backgroundColor: React.CSSProperties
  showButton?: boolean
  showBackground: boolean
  backgroundImage?: string

  columnNumber?: 3 | 2 | 1
}

class BarChart extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      redirect: false,
    }
  }

  generateColumns() {
    if (this.props.columnNumber === 2) {
      return (
        <div className="row">
          <div className="col-lg-6">f</div>
          <div className="col-lg-6">f</div>
        </div>
      )
    } else if (this.props.columnNumber === 1) {
      return (
        <div className="row">
          <div className="col-lg-12">f</div>
        </div>
      )
    } else {
      return (
        <div className="row">
          <div className="col-lg-4">f</div>
          <div className="col-lg-4">f</div>
          <div className="col-lg-4">f</div>
        </div>
      )
    }
  }

  render() {
    return (
      <div
        className={`info-tile ${this.props.showBackground ? 'coin-bg' : ''}`}
      >
        <h1>Calculate your profits </h1>
        <p>
          Coin Profit is service that will inform profit made from any time
          period. Analyse your previous transactions using a data visualisation
          dashboard and save your history to look back on later.
        </p>

        {this.props.showButton ? (
          <div className="cta hidden-xs">
            <button>Learn more </button>
            <button> Learn more </button>
          </div>
        ) : null}
      </div>
    )
  }
}

export default BarChart
