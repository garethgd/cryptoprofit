import * as React from 'react'
import { ClipLoader } from 'react-spinners'
import { Redirect } from 'react-router-dom'
import { Bar, Line } from 'react-chartjs-2'
import { Total }  from '../../types/Total'
import { CoinHistory } from '../../types/CoinHistory';
import { CoinInfo } from '../../types/CoinInfo';
import DatePicker from 'react-datepicker';
import { Moment } from 'moment';
import * as moment from 'moment'

export type State = {
  redirect: boolean
  startDate: Moment;
  currentDate: Moment;
  dateHistory: Date[];
  currentFilter: string
  selectedCoin: string,
}

export type Props = {
  total: Total
  coinTypes: CoinInfo[]
  onDateChange: (date: Moment) => void
  isLoading: boolean | undefined
  onCoinChange: (coinType: string, currentDate: Moment, type: string) => void
  lineChartData: CoinHistory[]
  comparisonData: CoinHistory[]
}

class LineChart extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      redirect: false,
      dateHistory: [],
      currentDate: moment(),
      selectedCoin: "",
      startDate: moment(),
      currentFilter: "low"
    }
  }

  onFilterChange(e: React.ChangeEvent<HTMLSelectElement>){
    this.setState({currentFilter: e.target.value})
  }

  onDateChange(date: Moment){
    this.setState({startDate: date, currentDate: date})
    this.props.onDateChange(date)
  }

  onSymChange(e) {
    this.setState({
      selectedCoin: e.target.value,
    });

    this.props.onCoinChange(e.target.value, this.state.currentDate, "linechart")
  }

componentDidMount() {
}

sortDates(){
  const lineChartData = this.props.lineChartData;
  const dates: CoinHistory[] = [];

  this.props.lineChartData.map(history => dates.push(history))

  dates.sort((a, b) => {
    return a.time - b.time

});

  const formatted = dates.map(info => moment(info.time).format("HH:MM:SS"));

  return formatted
  
}

render() {
  const coinTypesExist = this.props.coinTypes.length > 0;
  const currentOrFirstCoin =  this.state.selectedCoin ? this.state.selectedCoin :  this.props.coinTypes[0] ?  this.props.coinTypes[0].CoinName : "";

  const newOptions = {
    maintainAspectRatio: false,
    legend: {
      labels: {
        fontColor: 'white',
        fillStyle: 'white',
        margin: 20 + 'px',
        fontFamily: `"Roboto Mono",Helvetica,Arial,sans-serif`,
      }
  },
    scales: {
      yAxes: [{
          ticks: {
               // Include a dollar sign in the ticks
            callback: (value, index, values) => {
              return `$` + new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(value)
            },
          fontColor: "white",
          fontFamily: "Roboto Mono",
          fontSize: 12,
          beginAtZero: true
          }
      }],
      xAxes: [{
        time: {
          unit: 'hour'
      },
          ticks: {
              fontColor: "white",
              fontStyle: "lighter",
              fontFamily: "Roboto Mono",
              fontSize: 12,
              beginAtZero: true
          }
      }]
  }
}

  let data = {
    labels:  this.sortDates(),
    datasets: [
      {
        label: this.props.total.symbol,   
        fillColor: "white",
        strokeColor: "rgba(255,255,255, 0.9)",
        highlightFill: "rgba(255,255,255, 0.9)",
        highlightStroke: "rgba(255,255,255, 0.9)",
        data:  [...this.props.lineChartData.map(history => history[this.state.currentFilter.toLowerCase()])],
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            
        ],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
    },

    {
      label: `${currentOrFirstCoin}`,   
      fillColor: "rgba(255,255,255, 0.9)",
      strokeColor: "rgba(220,220,220,0.8)",
      highlightFill: "rgba(220,220,220,0.75)",
      highlightStroke: "rgba(220,220,220,1)",
      
      data:  [ ...this.props.comparisonData.map(history => history[this.state.currentFilter.toLowerCase()])],
      backgroundColor: [
        'rgba(54, 162, 235, 0.2)', 
      ],
      borderColor: [
        'rgba(54, 162, 235, 1)',
      ],
      borderWidth: 1
  },
  ]
}

    return ( 
      <div className="linechart panel">

      <div className="inputs">
      <label className="pt-label .modifier">
        Volume 
        <div className="pt-select">
          <select onChange={(e) => this.onFilterChange(e)}>
            <option>Choose historical data...</option>
            <option value="high">High</option>
            <option value="low">Low</option>
            <option value="open">Open</option>
          </select>
        </div>
      </label>

        <label className="pt-label .modifier">
                Comparison Coin 
                <div className="pt-select">
                <select
                      value={
                        this.state.selectedCoin
                          ? this.state.selectedCoin
                          : coinTypesExist ? this.props.coinTypes[0].FullName : ""
                      }
                      onChange={e => this.onSymChange(e)}
                      name="coin-type"
                >
                    <option > Choose a Coin </option>
                        {this.props.coinTypes
                          ? this.props.coinTypes.map((coin, index) => {
                              return <option value={coin.Symbol} key={index}> {coin.CoinName} </option>;
                            })
                          : null}
                </select>
                </div>
              </label>

          <label className={"pt-label .modifier date-picker"}>
          <span> Date </span>
          <DatePicker
                selected={this.state.startDate}
                onChange={(date: Moment) => this.onDateChange(date)}
          />
          </label>
          </div>
         {this.props.isLoading && this.props.coinTypes ? (
          <div className="loader">
            <ClipLoader color={'white'} loading={this.props.isLoading} />
          </div>
        ) : (
      <div>
            <Line
              data={data}
              width={100}
              height={350}
              options={
                newOptions  
              }
            />
     </div>)}
     </div>
      )
}
}

export default LineChart
