import * as React from 'react'
import { Link } from 'react-router-dom'
import SocialBuffer from 'react-icons/lib/io/calculator'
import AccountCircle from 'react-icons/lib/md/account-circle'
import FaNewspaperO from 'react-icons/lib/fa/newspaper-o'
import FaHistory from 'react-icons/lib/fa/history'
import SocialButtons from '../utils/socialButtons'
import AccountBox from 'react-icons/lib/md/account-box'
import {
  Menu,
  MenuItem,
  MenuDivider,
  Position,
  Button,
  Popover,
} from '@blueprintjs/core'

export type State = {}

export type Props = {
  isAuthenticated: boolean
  userEmail?: string
  onLocationChange: () => void
}

declare var require: any

class Header extends React.Component<Props> {
  constructor(props: Props) {
    super(props)
    this.state = { date: new Date() }
  }

  private handleClick(e: MouseEvent) {
    this.props.onLocationChange()
  }

  render() {
    const logo = require('../assets/logo.svg') as string;
    const inMobile = window.innerWidth < 750;
    const menu = (
      <Menu>
        <Link to={'/history'}>
          <MenuItem
            icon="new-text-box"
            onClick={e => this.handleClick(e)}
            text="History"
          />
        </Link>
        <MenuItem
          icon="new-object"
          onClick={e => this.handleClick(e)}
          text="Share"
        />
        <MenuItem
          icon="new-link"
          onClick={e => this.handleClick(e)}
          text="New link"
        />
        <MenuDivider />
        <MenuItem text="Settings..." icon="cog" />
      </Menu>
    )
    return (
      <div className="header">
        <header>
        <Link aria-label="home" to={'/home'}>
          <div className="logo">
            <img
              style={{
                height: 25 + 'px',
                color: 'white',
                marginRight: 15 + 'px',
                marginBottom: 10 + 'px',
              }}
              src={logo}
            />
            Coin Profit<sup>beta</sup>
          </div>
          </Link>
      
          {!this.props.isAuthenticated ? (
        <div>
        {}
        </div>
        ) : (
          <div id="email-greeting" className="email-greeting">
            <span> Hello, {this.props.userEmail} </span>
          </div>
          
        )}
          <div className="right-icons">
            <Link aria-label="logout" to={'/logout'}>
              <AccountBox className="header-icon" />
              <p>Logout</p>
            </Link>
            <div>
                <Link to={'/login'}>
                  <AccountCircle className="header-icon" />
                  <p className="advert">Login</p>{' '}
                </Link>

              </div>
            <div>
              <Link to={'/home'}>
                <SocialBuffer className="header-icon" />
                 Calculate 
              </Link>
            </div>
          </div>
          <nav className="menuList">

           <Link to={'/home'}>
            <SocialBuffer className="header-icon" />
            <p> Calculate </p>
          </Link>
            <Link to={'/home'}>
              <AccountCircle className="header-icon" />
              <p className="advert">Go Premium</p>{' '}
            </Link>
            {this.props.isAuthenticated ? (
              <div>
               <div>
                <Link to={'/login'}>
                  <AccountCircle className="header-icon" />
                  <p className="advert">Login</p>{' '}
                </Link>

              </div>
                        
              <div>
                <Link to={'/history'}>
                  <FaHistory className="header-icon" />
                  <p className="advert">History</p>{' '}
                </Link>
              </div>
              </div>) : null }
            
            <div>
                <Link to={'/p/1'}>
                  <FaNewspaperO className="header-icon" />
                  <p className="advert">Blog</p>{' '}
                </Link>
              </div>
          </nav>
          <SocialButtons url={'https://crypto-profit.org'} />
        </header>
      </div>
    )
  }
}

export default Header
