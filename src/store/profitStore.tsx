import axios from 'axios';
import { Moment } from 'moment';

export const getCurrentPrice = async function(coinSym: string, currency: string) {
    const res = await axios.get(
        `https://rocky-bayou-96357.herokuapp.com/https://min-api.cryptocompare.com/data/price?fsym=${coinSym}&tsyms=${currency}`
      )
   
      return res;
}

export const getPreviousPrice = async function(coinSym: string, currency: string, date: Moment) {
    const res = await axios.get(
      `https://rocky-bayou-96357.herokuapp.com/https://min-api.cryptocompare.com/data/pricehistorical?fsym=${coinSym}&tsyms=${currency},USD,EUR&ts=${date.unix()}&extraParams=your_app_name`
    )
 
    return res;
   }
