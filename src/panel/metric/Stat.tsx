import * as React from 'react'

const Stat = (props) => (
    <div className="grid-item stats panel" style={{minHeight: 50 + "px"}}>
    <div>
      <h1 className={`${props.stylingClass}`}> {props.value} {props.currency} </h1>
      <h4>{props.label}</h4>
    </div>
</div>
)

export default Stat;