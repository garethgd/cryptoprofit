// babel.config.js
module.exports = {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            node: 'current',
          },
        },
      ],
    ],

    "env": {
        "start": {
          "presets": [
            "react-hmre"
          ]
        },
        "test": {
          "presets": ["es2015", "react", "stage-0"]
        }
    }
  };