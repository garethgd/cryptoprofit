import * as React from 'react'
import { ClipLoader } from 'react-spinners'
import  Stat  from './Stat';
import { Redirect } from 'react-router-dom'
import { Total }  from '../../types/Total'
export type State = {
  redirect: boolean
}

export type Props = {
  data: any
  total: Total
}

class ProfitStats extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      redirect: false,
    }
  }

  render() {
    const total = this.props.total
    const gainProfit = total.gainPercent && total.newSP;
    const unableToCalculate = total.gainPercent === Number.POSITIVE_INFINITY;

    let currencyTotal
    let euroTotal
    let USDTotal

    if (total.CP && total.values) {
      currencyTotal = total.values[`${total.currency}`] * total.CP
      euroTotal = total.values[`EUR`]
      USDTotal = total.values[`USD`]
    }

    return (
      <div>
     
        <h3> Today your {total.symbol} is worth: </h3>
        <div className="profit-values">
              {/*<Stat value={gainProfit ? total.newSP!.toFixed(3) : 0} stylingClass={'red3'} label={total.currency || "no values"} />*/}
              <Stat 
               value={unableToCalculate ? 'Could not calculate profit' : total.gainPercent ?  total.gainPercent.toFixed(2) : total!.lostPercent!.toFixed(3)} 
               currency="%" 
               stylingClass={gainProfit ? "red5" : "red2"} 
               label={gainProfit ? "profit" : "loss"} 
              />        
             )
          <div className="grid-item stats">
            {(total.lostPercent || total.gainPercent) && total.newSP ? (
              <div>
                <Stat value={total.newSP.toFixed(3)} stylingClass={'red3'} label={total.currency} />
              </div>
            ) : null}
          </div>
        </div>
          </div> )
  }
}

export default ProfitStats
