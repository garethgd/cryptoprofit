import * as React from 'react'
import { ClipLoader } from 'react-spinners'
import { Redirect } from 'react-router-dom'
import  Stat  from './Stat';
import { Total }  from '../../types/Total'
export type State = {
  redirect: boolean
}

export type Props = {
  data: any
  total: Total
}

class PurchaseStats extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      redirect: false,
    }
  }

  render() {
    const total = this.props.total
    let currencyTotal
    let euroTotal
    let USDTotal

    if (total.CP && total.values) {
      currencyTotal = total.values[`${total.currency}`] * total.CP
      euroTotal = total.values[`EUR`]
      USDTotal = total.values[`USD`]
    }

    return (
      <div >
          <h3> Value at time of purchase:</h3>
          <div className="purchase-time">
         { total.coinAmount ?  
           <Stat label={total.symbol} value={total.coinAmount} stylingClass={"red1"}  />
           : null }
          {total.currency! === 'eur' ? (

            <Stat label={total.currency} value={currencyTotal.toFixed(3)} stylingClass={"red2"}  /> ) : null}

             <Stat label={"EURO"} value={euroTotal ? (euroTotal * parseInt(total!.coinAmount!, undefined)).toFixed(3) : euroTotal} stylingClass={"red1"}  />
             <Stat label={"USD"} value={USDTotal ? (USDTotal * parseInt(total!.coinAmount!, undefined)).toFixed(3) : USDTotal} stylingClass={"red2"}  />
        </div>
          </div> )
  }
}

export default PurchaseStats
