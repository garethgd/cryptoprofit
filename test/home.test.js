import React from 'react';
import { shallow } from 'enzyme';
import Home from '../src/home/home'



const mockState = {
  products: [],
  createMode: false,
  snackbarOpen: false,
  productSelected: false,
  loading: false,
  filteredProducts: [],
};

const mockProps = {
  products: [],
  snackbar: {
    message: '',
    open: false,
  },
  getProducts: () => {},
};

it('correctly enters create mode when clicking plus icon', () => {

 console.log(Home)
  const wrapper = shallow(<MainPage {...mockProps} />);
  wrapper.find('[id="create-product-btn"]').simulate('click');
  expect(wrapper.state()).toEqual({ ...mockState, createMode: true });
});
