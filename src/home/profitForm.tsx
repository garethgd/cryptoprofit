import * as React from 'react'
import { Moment } from 'moment'
import { CoinInfo } from '../types/CoinInfo'
import { ReactSelect } from '../utils/react-select-dropdown'
import DatePicker from 'react-datepicker'
import FaBitcoin from 'react-icons/lib/fa/bitcoin'
import * as moment from 'moment'

export type State = {
  startDate: Moment
  isLoading: boolean
  form: { priceEntered: boolean; errorMsg: string }
  selectedCoinType: ''
  selectedSymbol: string
  selectedCurrency: string
  selectedPrice: string
  selectedOption: string | null,
}

export type Props = {
  onSearch: (
    price: string,
    image: string,
    date: Moment,
    coinSym: string,
    currencySelected: string
  ) => void
  coinNames: string[]
  coinTypes: CoinInfo[]
}

class ProfitForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      form: { priceEntered: false, errorMsg: '' },
      isLoading: true,
      selectedSymbol: '',
      selectedCoinType: '',
      selectedOption: null,
      selectedCurrency: 'EUR',
      selectedPrice: '',
      startDate: moment(),
    }
  }

  componentDidMount() {
    this.setState({
      selectedSymbol: this.props.coinNames[0],
    })
  }

  formIsValid() {
    if (!this.state.form.priceEntered) {
      this.setState({
        form: {
          errorMsg: 'You need to enter an amount',
          priceEntered: this.state.form.priceEntered,
        },
      })
      return false
    } else {
      return true
    }
  }

  onSubmit() {
    let coins = this.props.coinTypes
    let matchedCoin = coins.find(coin => {
      return coin.CoinName === this.state.selectedSymbol
    })

    if (this.formIsValid()) {
      this.props.onSearch(
        this.state.selectedPrice,
        matchedCoin!.ImageUrl,
        this.state.startDate,
        matchedCoin!.Symbol,
        this.state.selectedCurrency
      )

      // <Redirect push={true} to="/results" />
    }
  }

  onSelectedCurrency(e) {
    this.setState({
      selectedCurrency: e.target.value,
      form: {
        currencyEntered: true,
        errorMsg: '',
        priceEntered: this.state.form.priceEntered,
      },
    })

    if (e.target.value === '') {
      this.setState({
        selectedPrice: e.target.value,
        form: {
          currencyEntered: false,
          priceEntered: this.state.form.priceEntered,
          errorMsg: 'You need to enter a currency',
        },
      })
    }
  }
  onPriceChange(e) {
    this.setState({
      selectedPrice: e.target.value,
      form: {
        priceEntered: true,
        errorMsg: '',
      },
    })

    if (e.target.value === '') {
      this.setState({
        selectedPrice: e.target.value,
        form: {
          priceEntered: false,
          errorMsg: 'You need to enter an amount',
        },
      })
    }
  }

  onSymChange(e) {
    this.setState({
      selectedSymbol: e.target.value,
    })
  }

  handleChange(date: Moment) {
    this.setState({
      startDate: date,
      
    })
  }

  _buildLinkHref() {
    if (this.state.form.priceEntered) {
      return '/results'
    } else {
      return ''
    }
  }

  handleChangee = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  }

  render() {
     
    const { selectedOption } = this.state;
    return (
      <div>
        <div style={{ textAlign: 'center' }}>
          <FaBitcoin className="logo" />
        </div>
        <h3> Enter information from a previous digital currency purchase.</h3>
        <label>Coin Type </label>
        <ReactSelect
            title={"Hello"}
            options={this.props.coinNames} 
            onSymChange={(e) => this.onSymChange(e)}  
        />
        
        <div className="price">
          <label>
            Amount of {this.state.selectedSymbol || 'Coin'} bought.
          </label>
          <input
            required={true}
            onChange={e => {
              this.onPriceChange(e)
            }}
            type="text"
            name="price"
          />
        </div>
        <div className="currency">
          <label>Currency </label>
          <select
            onChange={e => this.onSelectedCurrency(e)}
            name={'currency'}
            value={this.state.selectedCurrency}
          >
            <option value="EUR" key={'EUR'}>
              EUR
            </option>
            <option value="USD" key={'USD'}>
              USD
            </option>
          </select>
        </div>
        <label>Date </label>
        <DatePicker
          selected={this.state.startDate}
          onChange={(date: Moment) => this.handleChange(date)}
        />
        <p className="errorMsg">
          {this.state.form.errorMsg ? this.state.form.errorMsg : null}
        </p>
        <div className="submit">
          
            <button onClick={() => this.onSubmit()} type="submit">
              Enter
            </button>
         
        </div>
      </div>
    )
  }
}

export default ProfitForm
