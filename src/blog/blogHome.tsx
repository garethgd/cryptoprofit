import { Component } from 'react'
import * as React from 'react'
import { Redirect, Link } from 'react-router-dom'
import { ClipLoader } from 'react-spinners'
import  BlogPost from './blogPost'
import * as contentful from 'contentful'
import {
  Menu,
  MenuItem,
  MenuDivider,
  Position,
  Card,
  Elevation,
  Button,
  Popover,
} from '@blueprintjs/core'

export type State = {
  loaded: boolean
  resp: any,
  items: any
}

export type Props = {
  match: { params: { page: string } }
  
}

class BlogHome extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { loaded: false, resp: [], items: [{fields: {}}] }

    var client = contentful.createClient({
      space: 'sc1au1oyxitf',
      accessToken: '95f4efc8fbe86fea1af76ab02208b323376de0efcdf4aaaff9f32f8238917efd' })
    
      client.getEntries().then(entries => {
        entries.items.forEach(entry => {
          if (entry.fields) {
            let posts: any = [];
            posts.push(entry)
            console.log(entry.fields)
            console.log(entry)
            this.setState({items: posts, loaded: true})
          }
        })
      })
  }

  fetchPosts(page) {
    /*
    butter.post.list({ page: page, page_size: 10 }).then(resp => {
      this.setState({
        loaded: true,
        resp: resp.data,
      })
    })*/
  }

  componentWillMount() {
    let page = this.props.match.params.page || 1
    this.fetchPosts(page)
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ loaded: true })
    let page = nextProps.match.page || 1
    this.fetchPosts(page)
  }

  render() {
    if (this.state.loaded) {
     // const { next_page, previous_page } = this.state.resp.meta

      return (
        <div className={'container blog-home'}>
       {this.state.items ?
          this.state.items.map(post => {
            return (
              <div key={post.fields.path} className="col-md-3 blog-listing ">
                <Link to={`/post${post.fields.path}/${post.sys.id}`}>
                  <Card className="panel" interactive={true} elevation={Elevation.TWO}>
                    <h5>
                      <a href="#">{post.fields.title}</a>
                    </h5>
            {/* <p>Card content</p>*/}
                    <Button>View Post</Button>
                  </Card>
                </Link>
              </div>
            )
          }) : null}

          <br />
</div>
      )
    } else {
      return  (
        <div className="loader">
        <ClipLoader color={'white'} loading={true} />
      </div>)
    }
  }
}

export default BlogHome
