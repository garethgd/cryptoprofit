import * as React from 'react'
import { ClipLoader } from 'react-spinners'
import { Redirect } from 'react-router-dom'
import { Total }  from '../../types/Total'
import { CoinInfo } from '../../types/CoinInfo'
import axios from 'axios'

export type State = {
  popularCoins: Partial<CoinInfo>[]
  coinPrices: Object[]
}

export type Props = {
    coinList: CoinInfo[];
  
}

class CoinTile extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      popularCoins: [],
      coinPrices: []
    }
  }

  componentDidMount(){
      const coinList = this.props.coinList;
      let matchedCoins: CoinInfo[]
      let names = [ "Bitcoin", "Ethereum", "EOS", "BitCoin Cash"];
   
     axios.get(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,EOS,BCH&tsyms=BTC,USD,EUR`).then(res => {

         const coinPrices =  Object.assign({}, res.data)

         matchedCoins = coinList.filter(coin =>  coin.Symbol.match(/^(BTC|ETH|EOS|BCH)$/) )
         matchedCoins.forEach((coin, index) => {
          Object.assign(coin, coinPrices[coin.Symbol] )
         });
        this.setState({
            coinPrices: coinPrices,
            popularCoins: matchedCoins,
          })
     
      }) 
    
  }

  render() {
      const popularCoins = this.state.popularCoins;
      const coinPrices = this.state.coinPrices;
     
    return (
      <div className="coin-tiles">
         <h1>Popular Digital Currencies</h1> 
      {popularCoins.map((coin, index) => {
          return (
            <div key={index} className="tile panel">
              {coin.ImageUrl ? <img src={`https://www.cryptocompare.com${coin.ImageUrl}`} /> : null}
              
                <div className="header">
                   <h1>{coin.FullName}</h1>
                   <span>Market Open/ Today </span>
                </div>
                <h2>{coin.CoinName} / U.S. DOLLAR</h2>
      
                <div className="info">
                <h3>{coin.USD}</h3>
                </div>
                </div>
          )
      })}
     
          </div> 
        )
  }
}

export default CoinTile
 