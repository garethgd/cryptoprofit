
   export interface SearchHistory {
        id: number,
        coinType: string;
        currency: string;
        date: number;
        profit: boolean;
        profitPercent: string;
        lossPercent: string;
        owner:{ }
    }
