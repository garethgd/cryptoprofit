import * as React from 'react';
import {
  FacebookShareButton,
  FacebookIcon,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  TwitterIcon,
  TelegramShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  PinterestShareButton,
  VKShareButton,
  OKShareButton,
  RedditIcon,
  RedditShareButton,
  TumblrShareButton,
  LivejournalShareButton,
  MailruShareButton,
  ViberShareButton,
  WorkplaceShareButton,
  EmailShareButton,
  EmailIcon
} from 'react-share';

export type State = {
}

export type Props = {
    url: string
}

class SocialButtons extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)
    this.state = {

    }
  }

  render() {
    let { url } = this.props;
    url = 'https://coin-profit.org/' + url
    const inMobile = window.innerWidth < 600;
    const buttonStyle = {
      background: 'none',
      color: 'white',
      border: 1 + 'px solid white',
    }
  
    return (
         <div id="buttons">
           <FacebookShareButton
            url={url}
            quote={url}
           >
            <FacebookIcon
              size={32}
              round={true} 
            />
           </FacebookShareButton>
    
          <TwitterShareButton url={url}>
          <TwitterIcon
            size={32}
            url={url}
            round={true} 
          />
           </TwitterShareButton>

            <WhatsappShareButton url={this.props.url}>
          <WhatsappIcon
            size={32}
            url={url}
            round={true} 
          />
           </WhatsappShareButton>
    
        <RedditShareButton url={this.props.url}>
          <RedditIcon
            size={32}
            url={url}
            round={true} 
          />
          </RedditShareButton>
         
          <EmailShareButton url={this.props.url}>
           <EmailIcon
            size={32}
            url={url}
            round={true} 
           />
         </EmailShareButton>
        </div>
   
    )
  }
}

export default SocialButtons
