import * as React from 'react'
import AdSense from 'react-adsense'
import { ClipLoader } from 'react-spinners'
import { Tabs, Tab } from 'react-bootstrap'
import { SearchHistory } from '../types/SearchHistory'
import { CoinHistory } from '../types/CoinHistory'
import PieChart from '../panel/pie/pieChart'
import LineChart from '../panel/lineChart/lineChart'
import BarChart from '../panel/barchart/barChart'
import { CoinPrice } from '../types/CoinPrice'
import { CoinInfo } from '../types/CoinInfo'
import axios from 'axios'
import { AxiosPromise } from 'axios'
import ProfitStats from '../panel/metric/profitStats'
import PurchaseStats from '../panel/metric/purchaseStats'
import { Total } from '../types/Total'
import { Button, Card, Elevation, Tooltip, Position } from '@blueprintjs/core'
import { AppToaster } from '../utils/Toaster'
import { Moment } from 'moment'

export type State = {
  loading: { barChart?: boolean; lineChart?: boolean }
  isAuthenticated: boolean
  coinTypes: CoinInfo[]
  coinNames: string[]
  barChartData: CoinPrice[]
  lineChartData: { initialData: CoinHistory[]; comparisonData: CoinHistory[] }
  barChartFilters: { coinToCompare: CoinInfo[] }
}

export type Props = {
  data: Total | null;
  error: { errorHappened: boolean; errorMsg: string }
  loading: boolean
  onSaveHistory: (history: Total) => void
  total: Total
}

class Results extends React.Component<Props, State> {
  public static defaultProps: Partial<Props> = {
    total: {
      coinAmount: '30',
      gainPercent: 40,
      currency: 'EUR',
      symbol: 'ETH',
      newCP: 30,
      CP: 40,
      newSP: 50,
      SP: 70,
      lostPercent: 80,
    },
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      loading: { barChart: true, lineChart: true },
      isAuthenticated: false,
      barChartFilters: { coinToCompare: [] },
      barChartData: [],
      lineChartData: { initialData: [], comparisonData: [] },
      coinTypes: [],
      coinNames: [],
    }
  }

  componentWillMount() {
    this.getCoinHistory()
    this.getCoinTypes()
    this.getCoinCompare()
  }

  private getCoinTypes() {
    let coins: CoinInfo[] = []
    let coinNames: string[] = []

    axios
      .get(
        'https://rocky-bayou-96357.herokuapp.com/https://www.cryptocompare.com/api/data/coinlist/'
      )
      .then(response => {
        Object.keys(response.data.Data).forEach(function(key) {
          coins.push(response.data.Data[key])
        })

        coins.forEach(element => {
          coinNames.push(element.CoinName)
        })

        coinNames.sort()
        coins.sort((a, b) =>
          a.CoinName.toUpperCase().localeCompare(b.CoinName.toUpperCase())
        )

        this.getCoinCompare(coins[0].Symbol)
        this.setState({
          coinNames: coinNames,
          coinTypes: coins,
        })
      })
      .catch(function(error) {})
  }

  private async getCoinHistory(
    coinType?: string,
    timestamp?: Moment,
    chartType?: string
  ) {
    // this.setState({loading: { barChart: true, lineChart: true } })

    let coinToCompare = coinType ? coinType : this.props.total.symbol
    let res: AxiosPromise<any>

    if (timestamp) {
      this.setState({ loading: { barChart: false, lineChart: true } })
      res = axios.get(
        `https://min-api.cryptocompare.com/data/histoday?fsym=${coinToCompare}&tsym=USD&limit=60&aggregate=3&toTs=${timestamp.unix()}&e=CCCAGG`
      )
    } else {
      res = axios.get(
        `https://min-api.cryptocompare.com/data/histoday?fsym=${coinToCompare}&tsym=USD&limit=60&aggregate=3&e=CCCAGG`
      )
    }

    const response = await res

    let coinHistory = response.data.Data
    const linechartData = this.state.lineChartData

    if (coinType && chartType === 'barchart')
      this.setState({ barChartFilters: { coinToCompare: coinHistory } })
    else if (chartType === 'linechart') {
      linechartData.comparisonData = coinHistory
      this.setState({
        lineChartData: linechartData,
        coinNames: coinHistory,
        loading: { ...this.state.loading, lineChart: false },
      })
    } else {
      linechartData.initialData = coinHistory
      this.setState({
        lineChartData: linechartData,
        coinNames: coinHistory,
        loading: { ...this.state.loading, lineChart: false },
      })
    }
  }

  private async getCoinCompare(coinType?: string) {
    if (coinType) this.setState({ loading: { barChart: true } })

    let coinToCompare = coinType ? coinType : this.props.total.symbol
    const res = axios.get(
      `https://min-api.cryptocompare.com/data/price?fsym=${coinToCompare}&tsyms=${
        coinType ? coinType + ',' : ','
      }USD,EUR`
    )

    const response = await res

    let coinPrice = response.data
    const barChartData = this.state.barChartData

    if (coinType)
      this.setState({
        barChartData: barChartData,
        barChartFilters: { coinToCompare: coinPrice },
        loading: { barChart: false },
      })
    else {
      this.setState({ barChartData: coinPrice })
    }
  }

  private onSaveHistory(total) {
    let history = {
      coinType: total.symbol,
      currency: total.currency,
      values: this.props.data,
      date: Date.now(),
      profit: total.gainPercent ? true : false,
      gainPercent: total.gainPercent ? total.gainPercent : null,
      lossPercent: total.lossPercent ? total.lossPercent : null,
      owner: {},
      CP: total.CP,
      newCP: total.newCP,
      newSP: total.newSP,
      SP: total.SP,
    } as Total

    this.props.onSaveHistory(history)
    AppToaster.show({ message: 'History Saved.' })
  }
  render() {
    const total = this.props.total;
    const inMobile = window.innerWidth < 700
    const mobileAdd: React.CSSProperties = {
      width: 200 + 'px',
      margin: 15 + 'px' + 'auto',
    }

    return (
      <section>
        {this.props.loading ? (
          <div className="loader">
            <ClipLoader color={'white'} loading={this.props.loading} />
          </div>
        ) : (
          <div className="results">
            {this.props.error.errorHappened ? (
              <h1>{this.props.error.errorMsg}</h1>
            ) : (
              <div>
                  <Tooltip
                    content="Premium feature - coming soon!"
                    position={Position.RIGHT}
                  >
                  <button
                    disabled={true}
                    onClick={() => this.onSaveHistory(total)}
                  >
                    Save History
                  </button>
                  </Tooltip>
              <div className="result-panels">
            
                  <div className="stat-metric">
                      <h2> Coin values </h2>
                      <div
                        className={`${inMobile ? '' : 'panel'}`}
                      >
                        {total.coinImage ? (
                          <img
                            src={`https://www.cryptocompare.com${
                              total.coinImage
                            }`}
                          />
                        ) : null}
                          <PurchaseStats total={total} data={this.props.data} />
                        <ProfitStats total={total} data={this.props.data} />
                      </div>
                  </div>
                  
                <div className="stat-metric">
                    <h2> Compare coin by day </h2>
                    <BarChart
                      isLoading={this.state.loading.barChart}
                      barChartFilters={this.state.barChartFilters}
                      onCoinChange={(coinType: string, chartType: string) =>
                        this.getCoinCompare(coinType)
                      }
                      coinTypes={this.state.coinTypes}
                      barChartData={this.state.barChartData}
                      total={total}
                    />
                  </div>
                  <div className="stat-metric">
                      <h2> Profit/Loss </h2>
                      <div className="chart-wrapper panel">
                        <PieChart total={total} />
                    </div>
                    </div>

                  <div className={''}>
                    <h2> {total.symbol} over time </h2>
                    <LineChart
                      lineChartData={this.state.lineChartData.initialData}
                      isLoading={this.state.loading.lineChart}
                      coinTypes={this.state.coinTypes}
                      comparisonData={this.state.lineChartData.comparisonData}
                      onCoinChange={(
                        coinType: string,
                        date: Moment,
                        type: string
                      ) => this.getCoinHistory(coinType, date, type)}
                      onDateChange={(date: Moment) =>
                        this.getCoinHistory(undefined, date)
                      }
                      total={total}
                    />
                  </div>
              </div>
             </div>
              
            )}
          </div>
        )}
      </section>
    )
  }
}

export default Results
