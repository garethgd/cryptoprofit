import * as React from 'react'
import Autocomplete from "./autocomplete";
  
  export type Props = {
    options: string[],
    onSymChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    title: string
  }

  export type State = {
    options: {value: string, label: string}[],
    selected: string
  }

export class ReactSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
         options: [],
         selected: ''
        }
      }

   handleChange = (selected) => {
    this.setState({ selected });
    console.log(`Option selected:`, selected);
  }
    render() {
      return(
    <Autocomplete  onSymChange={(e) => this.props.onSymChange(e)} options={this.props.options} /> 
      )
  }

}

export default ReactSelect
