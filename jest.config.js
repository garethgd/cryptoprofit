module.exports = {
    "roots": [
      "<rootDir>/test"
    ],
    "transform": {
      "^.+\\.js?$": "ts-jest",
     
    },
    "testURL": 
      "http://localhost/",
    
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.jsx?$",
    "moduleFileExtensions": [
      "ts",
      "tsx",
      "js",
      "jsx",
      "json",
      "node"
    ],
    moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/src/$1',
      '^src/(.*)$': '<rootDir>/src/$1',
      '\\.(css)$': 'identity-obj-proxy', // add this
    },
  }
  