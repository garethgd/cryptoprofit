import * as React from 'react'
import { ClipLoader } from 'react-spinners'
import { Redirect } from 'react-router-dom'
import { Doughnut } from 'react-chartjs-2'
import { Total }  from '../../types/Total'

export type State = {
  redirect: boolean
}

export type Props = {
  total: Total
}

class PieChart extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      redirect: false,
    }
  }

  render() {
    const total = this.props.total
      let data = {
      datasets: [
        {
          data: [`${total.newCP}`, `${total.newSP}`],
          backgroundColor: [
            'rgba(20, 181, 204, 0.5)',
            `${total.gainPercent ? 'rgba(75, 192, 192, 0.2)' : 'rgba(255, 99, 132, 0.2)'}`,
          ],
          
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ],
        
          borderWidth: 0,
        },
      ],
      options: {
        responsive: true,
        segmentShowStroke: true,
        legend: {
          labels: {
            fontColor: 'white',
            fillStyle: 'white',
            margin: 20 + 'px',
            fontFamily: `"Roboto Mono",Helvetica,Arial,sans-serif`,
            pieceLabel: {
              render: 'percentage',
              fontColor: ['green', 'white', 'red'],
              precision: 2
            }
          }
      }
      },
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: ['Cost Price', `${total.gainPercent ? 'Profit' : 'Loss'}`],
    }
 return (
    <div className="pie-chart">
      <Doughnut data={data} options={data.options} height={292} />
    </div>
    )
  }
}

export default PieChart
