import * as React from 'react'
import {
    Panel
  } from 'react-bootstrap';

  const FAQ = () => (  
  <div className="container faq"> 
  <h1> FAQ </h1>
  <Panel>
      <Panel.Heading>
      <Panel.Title componentClass="h3">Q: What is Coin Profit?</Panel.Title>
      </Panel.Heading>
      <Panel.Body>A: Coin Profit is a simple crypto profit calculator that allows users to analyse the profits or losses of their previous transactions.</Panel.Body>
  </Panel>
  <Panel>
      <Panel.Heading>
      <Panel.Title componentClass="h3">Q: How can I view my profits?</Panel.Title>
      </Panel.Heading>
      <Panel.Body>A: When a date of transaction is entered Coin Profit allows you to see exchange rates of coin in comparison profits/loss from a particular transaction.</Panel.Body>
  </Panel>

  <Panel>
      <Panel.Heading>
      <Panel.Title componentClass="h3">Q: How is the data viewed?</Panel.Title>
      </Panel.Heading>
      <Panel.Body>A: Data is viewed by looking at the current cryptocurrency market using a wide range of graphs, including donut charts, bar charts and line charts.</Panel.Body>
  </Panel>

   <Panel>
      <Panel.Heading>
      <Panel.Title componentClass="h3">Q: Is this a bitcoin mining calculator?</Panel.Title>
      </Panel.Heading>
      <Panel.Body>A: No, this site provides profits from non mining crypto transactions.</Panel.Body>
  </Panel>

   <Panel>
      <Panel.Heading>
      <Panel.Title componentClass="h3">Q: So, I can see cryptocurrency prices?</Panel.Title>
      </Panel.Heading>
      <Panel.Body>A: Yes, The dashboard will show current prices from your crypto transaction.</Panel.Body>
  </Panel>

</div>
);
    
export default FAQ
