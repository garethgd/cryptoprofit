import * as React from 'react'
import { Link } from 'react-router-dom'
import { Total } from '../types/Total'
import { ClipLoader } from 'react-spinners'
import { SearchHistory } from '../types/SearchHistory'
import { FaTrash } from "react-icons/lib/fa";
import {
  Menu,
  MenuItem,
  MenuDivider,
  Position,
  Button,
  Popover,
} from '@blueprintjs/core'
import { Cell, Column, Table } from '@blueprintjs/table'
import { BaseExample } from '@blueprintjs/docs-theme'

export type State = {
  histories: SearchHistory[]
}

export type Props = {
  totalProfits: Total[]
  onSelectItem: (item: SearchHistory) => void;
}

class History extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {histories: [] }
  }

  componentDidMount() {
    const totalProfits = this.props.totalProfits
    let histories = Object.assign({}, totalProfits) as SearchHistory[];
    this.setState({histories: histories})
  }

  render() {
    const histories = Object.values(this.state.histories)
     const totalProfits = this.props.totalProfits
    return (
    histories.length < 1 ? (
          <div className="loader">
            <ClipLoader color={'white'} loading={true} />
          </div>
        ) : (
      <div className="history">
        <h1>Profit History </h1> 
        <table className="pt-html-table .modifier">
          <thead>
            <tr>
            <th />
              <th>Coin Type</th>
              <th>Currency</th>
              <th>Date</th>
              <th>P/L</th>
            </tr>
          </thead>
          <tbody>
            {histories.map((item, i) => (
              <tr key={item.date} onClick={() => this.props.onSelectItem(item)}>
                <td><FaTrash/></td>
                <td>{item.coinType}</td>
                <td>{item.currency}</td>
                <td>{item.date}</td>
                <td>{item.profit ? item.profitPercent : item.lossPercent}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
        )
      )
  }
}

export default History
