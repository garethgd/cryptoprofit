import { Component } from 'react'
import * as React from 'react'
import Helmet from 'react-helmet'
import AdSense from 'react-adsense'
import * as contentful from 'contentful'
import { ClipLoader } from 'react-spinners'
import  SocialButtons  from '../utils/socialButtons'
import * as Markdown from 'react-markdown'

export type State = {
  loaded: boolean
  resp: any
  post: any
}

export type Props = {
  match: { params: { page: string; slug: string, id: string } }
}

class BlogPost extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { loaded: false, resp: [], post: [] }
  }

  componentWillMount() {
    let slug = this.props.match.params.slug
    let id = this.props.match.params.id;

    var client = contentful.createClient({
      space: 'sc1au1oyxitf',
      accessToken: '95f4efc8fbe86fea1af76ab02208b323376de0efcdf4aaaff9f32f8238917efd' })
    
      client.getEntry("3aYEX6Q39YEOsQ0iaIg6i8")
      .then((entry) => {
        this.setState({
         loaded: true,
         post: entry.fields}) ; console.log(entry) } )
      .catch(console.error)
  }

  render() {
    const isMobile = window.innerWidth < 560;
 
    if (this.state.loaded) {
      const post = this.state.post;
      const image = post.postImage.fields.file.url;

      let timeToShow = post.date;
var t = new Date(timeToShow * 1000);
var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2);
      let date  = formatted;

      return (
        <div className="blog-content" style={{display: isMobile ? "block" : "" }}>
        <div className="banner" style={{background: `url('https:${image}')`, backgroundSize: "cover"}}>
      
        <h1>{post.title}</h1>
        <div className="sub-title"> <p>{} </p> </div>
        </div>
          
        <div className="container blog-post ">
      
        <div className="sidebar">
       
      {!isMobile ?  <SocialButtons url={this.props.match.params.slug} /> : null} 
          </div>
          <Helmet>
            <title>{post.title}</title>
            <meta name="description" content={post.content} />
            <link rel="canonical" href={`https://coin-profit.org/${this.props.match.params.slug}`} />
            <meta name="og:image" content={`https:${post.postImage.fields.file.url}`} />
          <meta name="twitter:card" content="summary"/>
          <meta name="twitter:site" content={post.title}/>
          <meta name="twitter:title" content={post.title}/>
          <meta name="twitter:description" content={post.content}/>
          <meta name="twitter:image" content={`https:${post.postImage.fields.file.url}`} />
          <meta property="og:type" content="website"/>
          <meta property="og:site_name" content="Crypto Calculator - Calculate your profits"/>
          <meta property="og:title" content={post.title}/>
          <meta property="og:description"  content={post.content}/>
          <meta property="og:image" content={`https:${post.postImage.fields.file.url}`} />
          <meta name="viewport" content="width=device-width, initial-scale=1"/>
          <meta property="og:title" content={post.title}/>
          <meta property="og:description" content={post.content}/>
          <meta property="og:site_name" content="Coin Profit"/>
          <meta property="og:image"  content={`https:${post.postImage.fields.file.url}`}/>    
          </Helmet>

          <Markdown source={post.content} />
      
          {isMobile ?  <div className="sidebar"> <SocialButtons url={this.props.match.params.slug} /></div> : null}
        </div>
        </div>
      )
    } else {
      return  (
      <div className="loader">
      <ClipLoader color={'white'} loading={true} />
    </div>)
    }
  }
}

export default BlogPost
