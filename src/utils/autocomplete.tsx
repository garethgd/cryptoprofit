import { Component, Fragment } from "react";
import * as React from 'react'

export type Props = {
  options: string[],
  onSymChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export type State = {
  options: string[],
  activeSuggestion: number,
  filteredSuggestions: string[],
  showSuggestions: boolean,
  userInput: string
}

class Autocomplete extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: ""
    };
  }

  componentDidMount() {
    const items = this.props.options.map(coin => { return coin })

    this.setState({
      options: items
    })
  }

  // Event fired when the input value is changed
  onChange = e => {
    const { options } = this.props;
    const userInput = e.currentTarget.value;

    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = options.filter(
      suggestion =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    // Update the user input and filtered suggestions, reset the active
    // suggestion and make sure the suggestions are shown
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });

  };

  // Event fired when the user clicks on a suggestion
  onClick = e => {
    // Update the user input and reset the rest of the state
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });

  };

  // Event fired when the user presses a key down
  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key, update the input and close the
    // suggestions
    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion]
      });
    }
    // User pressed the up arrow, decrement the index
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow, increment the index
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  private onSymChange(e) {
    const match = this.state.options.some(option => option === e.target.value)
    if (match) {
      this.props.onSymChange(e);
    }
  }

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;

    let suggestionsListComponent;

    if (true) {
      if (this.state.options.length > 0) {
        suggestionsListComponent = (
          this.state.options
            ? this.state.options.map((name, index) => {
              return <option key={index}>{name}</option>
            })
            : null
        );

      } else {
        suggestionsListComponent = (
          <option className="no-suggestions">
            <em>No suggestions, you're on your own!</em>
          </option>
        );
      }
    }

    return (
      <div>
        <input
          onChange={onChange}
          onInput={(e) => this.onSymChange(e)}
          placeholder="Search Coin"
          type="text"
          list="coins"
          onClick={onClick}
          onKeyDown={onKeyDown}
        />
        <datalist id="coins">
          {suggestionsListComponent}
        </datalist>
      </div>

    );
  }
}

export default Autocomplete;