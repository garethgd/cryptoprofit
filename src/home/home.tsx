import * as React from 'react'
import Header from '../header/header'
import Info from '../info/info'
import FaBitcoin from 'react-icons/lib/fa/bitcoin'
import DatePicker from 'react-datepicker'
import Results from '../results/results'
import ProfitForm from './profitForm'
import { Dashboard } from '../types/Dashboard'
import { CoinInfo } from '../types/CoinInfo'
import CoinTile from '../panel/coinTile/coinTile'
import AdSense from 'react-adsense'
import SocialButtons from '../utils/socialButtons'
import { ClipLoader } from 'react-spinners'
import { Redirect, Link, Route, RouteComponentProps, RouteProps } from 'react-router-dom'
import 'react-datepicker/dist/react-datepicker.css'
import { Moment } from 'moment'

import * as moment from 'moment'

export type Props = {
  onSearch: (
    price: string,
    image: string,
    date: Moment,
    coinSym: string,
    currencySelected: string
  ) => void
  fireBaseApp: any;
  coinTypes: CoinInfo[];
  coinNames: any[];
  isLoading: boolean;
  history?: any;
  location?: any;
  match?: any;
  getCoinTypes: () => void;
  onLocationChange?: () => void;
  canCalculate?: boolean;
  results: Dashboard
}

export type FormDetails = {
  price: string
  date: Moment
  coinSym: string
}

export type State = {
  coinTypes: CoinInfo[]
  coinNames: any[]
  coinSymbols: string[]
}

class Home extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      coinNames: [],
      coinTypes: [],
      coinSymbols: [],
    }
  }

  componentDidMount() {
    this.props.getCoinTypes()
  }

  componentWillReceiveProps(newProps: Props) {
    const differentCoinSelected = newProps.results.total.coinImage !== this.props.results.total.coinImage;
    if (newProps.canCalculate && differentCoinSelected) {
      this.props.history.push('/results')
    }

  }

  onSubmit(
    price: string,
    image: string,
    date: Moment,
    coinSym: string,
    currencySelected: string
  ) {

    this.props.onSearch(price, image, date, coinSym, currencySelected)

    // <Redirect push={true} to="/results" />
  }

  render() {
    const { results } = this.props
    const inMobile = window.innerWidth < 600
    const mobileAdd: React.CSSProperties = {
      width: 200 + 'px',
      margin: 15 + 'px' + 'auto',
    }

    if (this.props.canCalculate) {
    }

    const learnMore = (
      <Info
        backgroundColor={{ color: 'white' }}
        showButton={false}
        columnNumber={2}
        showBackground={true}
      />
    )

    const initialInfo = (
      <Info
        backgroundColor={{ color: 'white' }}
        showButton={false}
        columnNumber={3}
        showBackground={true}
      />
    )
    return (
      <section>
        {this.props.isLoading ? (
          <div ref={`homeRef`} className="loader">
            <ClipLoader color={'white'} loading={this.props.isLoading} />
          </div>
        ) : (
            <div ref={`homeRef`} className="home">
              <div className="wrapper">
                <CoinTile coinList={this.props.coinTypes} />

                <h1 style={{ display: 'none' }}>
                  Bitcoin Price Converter and Calculator
              </h1>
                <p style={{ display: 'none' }}>
                  Convert Bitcoin to and from world currencies.
              </p>
                <div className="profit-form">
                  <h1 className="title hidden"> Crypto Profit Calculator </h1>

                  <div
                    className="col-md-6 hidden"
                  >
                    <div style={{ textAlign: 'center' }}>
                      <FaBitcoin className="logo" />
                    </div>
                    <h2 className="desc">
                      Calculate all your crypto profits for bitcoin and alt coins
                      and display them a graph.
                  </h2>
                    <p>
                      Coin Profit is service that will simply tell you of your
                      profit from any time period.
                  </p>

                    <div className="row hidden-xs">
                      <a
                        className="submit"
                        href="mailto:dunnedev@jsdiaries.com?Subject=Advertising%query"
                      >
                        <button className="advert">Advertising </button>
                      </a>

                      <a
                        className="submit "
                        target="_blank"
                        href="https://www.coinbase.com/join/5a1d548ec5375b02137989f8"
                      >
                        <button className="advert"> Buy coins </button>
                      </a>
                    </div>

                    <span className="ion-social-bitcoin-outline" />
                  </div>
                  <div>
                    <h2> Enter Previous Purchase </h2>
                    <div className="previous-purchase panel">
                      <ProfitForm
                        onSearch={(
                          price: string,
                          image: string,
                          date: Moment,
                          coinSym: string,
                          currencySelected: string
                        ) =>
                          this.onSubmit(
                            price,
                            image,
                            date,
                            coinSym,
                            currencySelected
                          )
                        }
                        coinNames={this.props.coinNames}
                        coinTypes={this.props.coinTypes}
                      />
                    </div>
                  </div>
                </div>

              </div>
            </div>
          )}
      </section>
    )
  }
}

export default Home
